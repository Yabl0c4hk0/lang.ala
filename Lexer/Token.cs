﻿using lang.ala.Interpreter;

namespace lang.ala.Lexer
{
    public enum TokenType
    {
        Integer,
        Real,
        String,
        Bool,
        
        Identifier,
        KeyWord,

        Op,
        
        ParenL, // '('
        ParenR  // ')'
    }

    public class Token
    {
        public Pointer Pointer;
        public TokenType Type;
        public string Value;
        public Pointer Start;
        public Pointer End;


        public Token(TokenType type, Pointer start, Pointer end) =>
            Constructor(type, null, start, end);

        public Token(TokenType type, Pointer pointer) =>
            Constructor(type, null, pointer, pointer);

        public Token(TokenType type, string value, Pointer start, Pointer end) =>
            Constructor(type, value, start, end);

        public Token(TokenType type, string value, Pointer pointer) =>
            Constructor(type, value, pointer, pointer);

        private void Constructor(TokenType type, string value, Pointer start, Pointer end)
        {
            Type = type;
            Value = value;
            Start = new Pointer(start);
            End = new Pointer(end);
        }
        
        public override string ToString() => Value != null ? $"{Type}:{Value}" : $"{Type}";
    }
}