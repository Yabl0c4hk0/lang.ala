﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace lang.ala.Lexer
{
    public class Lexer
    {
        private const string Whitespaces = " \r\n\t";
        private const string Digits = "0123456789";
        private const string Letters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";

        private static readonly Dictionary<string, string> Ops = new Dictionary<string, string>
        {
            {"+", "PLUS"},
            {"-", "MINUS"},
            {"*", "MULTIPLY"},
            {"/", "DIVIDE"},
            {"%", "MOD"},
            {"=", "ASSIGNMENT"},
            {"!", "NOT"},
            {">", "GREATER"},
            {"<", "LESS"},
            {"+=", "PLUS_ASSIGNMENT"},
            {"-=", "MINUS_ASSIGNMENT"},
            {"*=", "MULTIPLY_ASSIGNMENT"},
            {"/=", "DIVIDE_ASSIGNMENT"},
            {"++", "INCREMENT"},
            {"--", "DECREMENT"},
            {"**", "POWER"},
            {"==", "EQUALS"},
            {">=", "GREATER_OR_EQUAL"},
            {"<=", "LESS_OR_EQUAL"},
            {"&", "AND"},
            {"&&", "LAZY_AND"},
            {"|", "OR"},
            {"||", "LAZY_OR"}
        };

        private static readonly Dictionary<string, TokenType> SimpleTokens = new Dictionary<string, TokenType>
        {
            {"(", TokenType.ParenL},
            {")", TokenType.ParenR}
        };

        private static readonly string[] KeyWords =
        {
            "if", "else", "int", "real", "bool", "string"
        };

        private readonly string _code;

        public readonly Token[] Tokens;
        public Error Error;

        private Pointer _pointer = new Pointer();

        private char CurrentChar => _pointer.Pos < _code.Length
            ? _code[_pointer.Pos]
            : '\0';

        private void Advance(int n = 1)
        {
            for (; n > 0; n--)
            {
                _pointer.Pos++;
                if (CurrentChar == '\n')
                {
                    _pointer.Line++;
                    _pointer.Char = 0;
                    continue;
                }

                _pointer.Char++;
            }
        }

        public Lexer(string code)
        {
            _code = code;
            Tokens = Tokenize().ToArray();
        }

        private IEnumerable<Token> Tokenize()
        {
            while (CurrentChar != '\0')
            {
                while (Whitespaces.Contains(CurrentChar)) Advance();
                if ((Digits + '.').Contains(CurrentChar))
                    yield return TokenizeNumber();
                else if ((Letters + '_').Contains(CurrentChar))
                    yield return TokenizeWord();
                else if (CurrentChar == '"')
                    yield return TokenizeString();
                else
                {
                    var finded = false;
                    for (var i = (int) MathF.Max(Ops.Keys.Max().Length, SimpleTokens.Keys.Max().Length); i > 0; i--)
                    {
                        var taken = Take(i);
                        var ptr = new Pointer(_pointer);
                        if (taken == null) continue;
                        var op = Ops.GetValueOrDefault(taken, null);
                        var tokenType = SimpleTokens.GetValueOrDefault(taken);
                        if (op != null)
                        {
                            Advance(i-1);
                            finded = true;
                            yield return new Token(TokenType.Op, op, ptr, _pointer);
                        }
                        else if (tokenType != 0)
                        {
                            Advance(i-1);
                            finded = true;
                            yield return new Token(tokenType, ptr, _pointer);
                        }

                        if (finded)
                        {
                            Advance();
                            break;
                        }


                        Console.WriteLine();
                    }

                    if (finded) continue;

                    TokenizeError(_pointer);
                    yield break;
                }
            }
        }

        private Token TokenizeNumber()
        {
            var start = new Pointer(_pointer);

            var num = "" + CurrentChar;
            Advance();

            while (CurrentChar != '\0' && (Digits + "._").Contains(CurrentChar))
            {
                if (CurrentChar == '.')
                {
                    if (num.Contains('.')) TokenizeError(start, _pointer);
                }

                while (CurrentChar == '_') Advance();
                num += CurrentChar;
                Advance();
            }

            if (num.First() == '.')
                num = '0' + num;
            if (num.Last() == '.')
                num += '0';

            return new Token(num.Contains('.') ? TokenType.Real : TokenType.Integer, num, start, _pointer);
        }

        private Token TokenizeWord()
        {
            var start = new Pointer(_pointer);

            var word = "";
            var pointer = new Pointer(_pointer);
            while (CurrentChar != '\0' && (Letters + Digits + '_').Contains(CurrentChar))
            {
                word += CurrentChar;
                pointer = new Pointer(_pointer);
                Advance();
            }

            _pointer = pointer;
            Token token;
            if (new[] {"true", "false"}.Contains(word))
                token = new Token(TokenType.Bool, word, start, _pointer);
            else if (KeyWords.Contains(word))
                token = new Token(TokenType.KeyWord, word, start, _pointer);
            else
                token = new Token(TokenType.Identifier, word, start, _pointer);
            Advance();
            return token;
        }

        private Token TokenizeString()
        {
            var start = new Pointer(_pointer);
            var s = "";
            Advance();
            while (!"\0\"".Contains(CurrentChar))
            {
                if (CurrentChar != '\\')
                    s += CurrentChar;
                else
                {
                    Advance();
                    var protect = CurrentChar switch
                    {
                        '\\' => "\\",
                        '\"' => "\"",
                        _ => null
                    };
                    if (protect != null)
                        s += protect;
                    else
                        TokenizeError(start, _pointer);
                }

                Advance();
            }

            Advance();
            return new Token(TokenType.String, s, start, _pointer);
        }

        private string Take(int count)
        {
            return _code.Length >= count + _pointer.Pos
                ? string.Join("", _code.Skip(_pointer.Pos).Take(count))
                : null;
        }

        private Token TokenizeError(Pointer pointer) => TokenizeError(pointer, pointer);


        private Token TokenizeError(Pointer start, Pointer end)
        {
            Error = new Error(_code, "TokenizeError", start, end);
            return null;
        }
    }
}