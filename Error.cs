﻿using System;
using System.Linq;
using lang.ala.Lexer;

namespace lang.ala
{
    public class Error
    {
        public Error(string text, string exception, Pointer pointer)
        {
            Console.WriteLine($@"{exception} (line:{pointer.Line} char:{pointer.Char})");
            DrawExceptedStrings(text, pointer, pointer);
        }

        public Error(string text, string exception, Pointer start, Pointer end)
        {
            Console.WriteLine(
                $@"{exception} (start line:{start.Line} char:{start.Char}; end line:{end.Line} char:{end.Char})");
            DrawExceptedStrings(text, start, end);
        }

        private static void DrawExceptedStrings(string text, Pointer start, Pointer end)
        {
            var lines = text.Split("\r\n").ToList();
            lines = lines.Take(end.Line).Skip(start.Line - 1).ToList();

            var starts = string.Join("", lines.First().Take(start.Char - 1));
            var middle = string.Join("", text.Take(end.Pos + 1).Skip(start.Pos));
            middle = middle == "" ? " " : middle;
            var ends = string.Join("", lines.Last().Skip(end.Char));
            

            Console.Write(starts);
            Console.BackgroundColor = ConsoleColor.Red;
            Console.Write(middle);
            Console.ResetColor();
            Console.WriteLine(ends);
        }
    }
}