﻿using System.Collections.Generic;
using System.Linq;
using lang.ala.Lexer;

namespace lang.ala.Interpreter
{
    public class VariablesTable
    {
        public VariablesTable Parent;
        public readonly List<Variable> Variables = new List<Variable>();

        public VariablesTable(VariablesTable parent = null)
        {
            Parent = parent;
        }


        public Variable Get(string name)
        {
            foreach (var variable in Variables.Where(variable => variable.Name == name))
                return variable;

            return new Variable(null, Value.Null);
        }
        
        public Value Set(string name, Value value)
        {
            for (var i = 0; i < Variables.Count; i++)
                if (Variables[i].Name == name)
                    Variables[i] = new Variable(name, value);
            return Value.Null;
        }
        
        public void Add(Variable variable)
        {
            Variables.Add(variable);
        }

    }
    public struct Variable
    {
        public string Name;
        public Value Value;

        public Variable(string name, Value value)
        {
            Name = name;
            Value = value;
        }
    }
}