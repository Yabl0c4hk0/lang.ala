﻿namespace lang.ala.Interpreter
{
    public class Context
    {
        public string Name;
        public Context Parent;
        public VariablesTable VariablesTable;

        public Context(string name, Context parent = null)
        {
            Name = name;
            Parent = parent;
        }
    }
}