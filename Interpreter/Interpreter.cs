﻿#nullable enable
using System;
using System.Globalization;
using System.Linq;
using lang.ala.Lexer;
using lang.ala.Parser;

namespace lang.ala.Interpreter
{
    public class Interpreter
    {
        private bool _error;
        private readonly string _code;

        public Interpreter(string code)
        {
            _code = code;
        }

        public Value Visit(INode node, Context context)
        {
            if (_error) return Value.Null;
            var type = GetType();
            var method = type.GetMethod("Visit" + node.GetType().Name);
            if (method == null) return Value.Null;
            var result = method.Invoke(this, new object?[] {node, context});
            if (result is Value res)
                return res;

            return Value.Null;

        }

        // ReSharper disable once UnusedMember.Global
        // ReSharper disable once UnusedParameter.Global
        public Value VisitIntegerNode(IntegerNode node, Context context) =>
            new Value(ValueType.Integer, node.Token.Value);

        // ReSharper disable once UnusedMember.Global
        // ReSharper disable once UnusedParameter.Global
        public Value VisitRealNode(RealNode node, Context context) => new Value(ValueType.Real, node.Token.Value);

        // ReSharper disable once UnusedMember.Global
        // ReSharper disable once UnusedParameter.Global
        public Value VisitBooleanNode(BooleanNode node, Context context) => new Value(ValueType.Bool, node.Token.Value);

        // ReSharper disable once UnusedMember.Global
        // ReSharper disable once UnusedParameter.Global
        public Value VisitStringNode(StringNode node, Context context) => new Value(ValueType.String, node.Token.Value);

        // ReSharper disable once UnusedMember.Global
        public Value VisitVariableNode(VariableNode node, Context context)
        {
            return context.VariablesTable.Get(node.Token.Value).Value;
        }

        // ReSharper disable once UnusedMember.Global
        public Value VisitBinaryOpNode(BinaryOpNode node, Context context)
        {
            var a = Visit(node.A, context);
            var op = node.Op;
            var b = Visit(node.B, context);
            var result = op.Value switch
            {
                "PLUS" when a.IsInteger && b.IsInteger => new Value(ValueType.Integer,
                    (a.AsInteger + b.AsInteger).ToString()),
                "PLUS" when a.IsInteger && b.IsReal => new Value(ValueType.Real,
                    (a.AsInteger + b.AsReal).ToString(CultureInfo.InvariantCulture)),
                "PLUS" when a.IsReal && b.IsInteger => new Value(ValueType.Real,
                    (a.AsReal + b.AsInteger).ToString(CultureInfo.InvariantCulture)),
                "PLUS" when a.IsReal && b.IsReal => new Value(ValueType.Real,
                    (a.AsReal + b.AsReal).ToString(CultureInfo.InvariantCulture)),
                "MINUS" when a.IsInteger && b.IsInteger => new Value(ValueType.Integer,
                    (a.AsInteger - b.AsInteger).ToString()),
                "MINUS" when a.IsInteger && b.IsReal => new Value(ValueType.Real,
                    (a.AsInteger - b.AsReal).ToString(CultureInfo.InvariantCulture)),
                "MINUS" when a.IsReal && b.IsInteger => new Value(ValueType.Real,
                    (a.AsReal - b.AsInteger).ToString(CultureInfo.InvariantCulture)),
                "MINUS" when a.IsReal && b.IsReal => new Value(ValueType.Real,
                    (a.AsReal - b.AsReal).ToString(CultureInfo.InvariantCulture)),
                "MULTIPLY" when a.IsInteger && b.IsInteger => new Value(ValueType.Integer,
                    (a.AsInteger * b.AsInteger).ToString()),
                "MULTIPLY" when a.IsInteger && b.IsReal => new Value(ValueType.Real,
                    (a.AsInteger * b.AsReal).ToString(CultureInfo.InvariantCulture)),
                "MULTIPLY" when a.IsReal && b.IsInteger => new Value(ValueType.Real,
                    (a.AsReal * b.AsInteger).ToString(CultureInfo.InvariantCulture)),
                "MULTIPLY" when a.IsReal && b.IsReal => new Value(ValueType.Real,
                    (a.AsReal * b.AsReal).ToString(CultureInfo.InvariantCulture)),
                "DIVIDE" when a.IsInteger && b.IsInteger => b.AsInteger != 0
                    ? new Value(ValueType.Real,
                        (a.AsInteger / b.AsInteger).ToString(CultureInfo.InvariantCulture))
                    : Value.Null,
                // ReSharper disable once CompareOfFloatsByEqualityOperator
                "DIVIDE" when a.IsInteger && b.IsReal => b.AsReal != 0
                    ? new Value(ValueType.Real,
                        (a.AsInteger / b.AsReal).ToString(CultureInfo.InvariantCulture))
                    : Value.Null,
                "DIVIDE" when a.IsReal && b.IsInteger => b.AsInteger != 0
                    ? new Value(ValueType.Real,
                        (a.AsReal / b.AsInteger).ToString(CultureInfo.InvariantCulture))
                    : Value.Null,
                // ReSharper disable once CompareOfFloatsByEqualityOperator
                "DIVIDE" when a.IsReal && b.IsReal => b.AsReal != 0
                    ? new Value(ValueType.Real,
                        (a.AsInteger / b.AsReal).ToString(CultureInfo.InvariantCulture))
                    : Value.Null,
                "POWER" when a.IsInteger && b.IsInteger => new Value(ValueType.Integer,
                    MathF.Pow(a.AsInteger, b.AsInteger).ToString(CultureInfo.InvariantCulture)),
                "POWER" when a.IsInteger && b.IsReal => new Value(ValueType.Real,
                    MathF.Pow(a.AsInteger, b.AsReal).ToString(CultureInfo.InvariantCulture)),
                "POWER" when a.IsReal && b.IsInteger => new Value(ValueType.Real,
                    MathF.Pow(a.AsReal, b.AsInteger).ToString(CultureInfo.InvariantCulture)),
                "POWER" when a.IsReal && b.IsReal => new Value(ValueType.Real,
                    MathF.Pow(a.AsReal, b.AsReal).ToString(CultureInfo.InvariantCulture)),
                "GREATER" when (a.IsInteger || a.IsReal) && (b.IsInteger || b.IsReal) =>
                new Value(ValueType.Bool, a.AsReal > b.AsReal ? "true" : "false"),
                "GREATER_OR_EQUAL" when (a.IsInteger || a.IsReal) && (b.IsInteger || b.IsReal) =>
                new Value(ValueType.Bool, a.AsReal >= b.AsReal ? "true" : "false"),
                "LESS" when (a.IsInteger || a.IsReal) && (b.IsInteger || b.IsReal) =>
                new Value(ValueType.Bool, a.AsReal < b.AsReal ? "true" : "false"),
                "LESS_OR_EQUAL" when (a.IsInteger || a.IsReal) && (b.IsInteger || b.IsReal) =>
                new Value(ValueType.Bool, a.AsReal <= b.AsReal ? "true" : "false"),
                "EQUALS" when a.Type == b.Type && (a.IsInteger || a.IsReal) && (b.IsInteger || b.IsReal) =>
                new Value(ValueType.Bool, a.Val == b.Val ? "true" : "false"),
                _ => Value.Null
            };
            return result.IsNull ? RuntimeError($"Cant use {op} with {a.Type} and {b.Type}", op.Start, op.End) : result;
        }


        // ReSharper disable once UnusedMember.Global
        public Value VisitLogicOpNode(LogicOpNode node, Context context)
        {
            var result = node.Op.Value switch
            {
                "AND" => Visit(node.A, context).Val == "true" & Visit(node.B, context).Val == "false"
                    ? "true"
                    : "false",
                "LAZY_AND" => Visit(node.A, context).Val == "true" && Visit(node.B, context).Val == "false"
                    ? "true"
                    : "false",
                "OR" => Visit(node.A, context).Val == "true" | Visit(node.B, context).Val == "false"
                    ? "true"
                    : "false",
                "LAZY_OR" => Visit(node.A, context).Val == "true" || Visit(node.B, context).Val == "false"
                    ? "true"
                    : "false",
                _ => null
            };

            return result != null ? new Value(ValueType.Bool, result) : Value.Null;
        }


        // ReSharper disable once UnusedMember.Global
        public Value VisitUnaryOpNode(UnaryOpNode node, Context context)
        {
            var factor = Visit(node.Factor, context);
            var op = node.Op;

            var result = op.Value switch
            {
                "PLUS" when factor.IsInteger => new Value(ValueType.Integer,
                    (+factor.AsInteger).ToString()),
                "PLUS" when factor.IsReal => new Value(ValueType.Integer,
                    (+factor.AsReal).ToString(CultureInfo.InvariantCulture)),
                "MINUS" when factor.IsInteger => new Value(ValueType.Integer,
                    (-factor.AsInteger).ToString()),
                "MINUS" when factor.IsReal => new Value(ValueType.Integer,
                    (-factor.AsReal).ToString(CultureInfo.InvariantCulture)),
                "NOT" when factor.IsBool => new Value(ValueType.Integer,
                    factor.Val == "true" ? "false" : "true"),
                _ => Value.Null
            };
            return result.IsNull ? RuntimeError($"Cant use {op} with {factor.Type}", op.Start, op.End) : result;
        }

        // ReSharper disable once UnusedMember.Global
        public Value VisitVarCreationNode(VarCreationNode node, Context context)
        {
            if (context.VariablesTable.Variables.Any(variable => variable.Name == node.Name.Value))
                return Value.Null;

            context.VariablesTable.Add(new Variable(node.Name.Value, Visit(node.Value, context)));

            return Value.Null;
        }

        // ReSharper disable once UnusedMember.Global
        public Value VisitVarAssignmentNode(VarAssignmentNode node, Context context)
        {
            if (context.VariablesTable.Variables.Any(variable => variable.Name == node.Name.Value))
                return context.VariablesTable.Set(node.Name.Value, Visit(node.Value, context));
            return Value.Null;
        }

        private Value RuntimeError(string exception, Pointer start, Pointer end)
        {
            _error = true;
            var _ = new Error(_code, $"RuntimeError: {exception}", start, end);
            return Value.Null;
        }
    }
}