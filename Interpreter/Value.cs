﻿namespace lang.ala.Interpreter
{
    public enum ValueType
    {
        Integer,
        Real,
        Bool,
        String,
        
        Null
    }

    public class Value
    {
        public readonly ValueType Type;
        public readonly string Val;
        
        public static Value Null => new Value(ValueType.Null, "null");

        public Value(ValueType type, string value)
        {
            Type = type;
            Val = value;
        }
        
        public override string ToString() => Val != null ? $@"{Type}:{Val}" : $@"{Type}";

        public bool IsInteger => Type == ValueType.Integer;
        public bool IsReal => Type == ValueType.Real;
        public bool IsBool => Type == ValueType.Bool;
        public bool IsString => Type == ValueType.String;
        public bool IsNull => Type == ValueType.Null;

        public int AsInteger => int.Parse(Val);
        public float AsReal => float.Parse(Val.Replace('.',','));
    }
}