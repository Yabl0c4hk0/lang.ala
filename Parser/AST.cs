﻿using lang.ala.Lexer;

namespace lang.ala.Parser
{
    public interface INode
    {
    }

    // ReSharper disable once ClassNeverInstantiated.Global
    public class IntegerNode : INode
    {
        public readonly Token Token;

        public IntegerNode(Token token)
        {
            Token = token;
        }

        public override string ToString() => $"{Token.Value}";
    }

    // ReSharper disable once ClassNeverInstantiated.Global
    public class RealNode : INode
    {
        public readonly Token Token;

        public RealNode(Token token)
        {
            Token = token;
        }

        public override string ToString() => $"{Token.Value}";
    }

    // ReSharper disable once ClassNeverInstantiated.Global
    public class BooleanNode : INode
    {
        public readonly Token Token;

        public BooleanNode(Token token)
        {
            Token = token;
        }

        public override string ToString() => $"{Token.Value}";
    }

    // ReSharper disable once ClassNeverInstantiated.Global
    public class StringNode : INode
    {
        public readonly Token Token;

        public StringNode(Token token)
        {
            Token = token;
        }

        public override string ToString() => $"String:\"{Token.Value.Replace("\"", "\\\"")}\"";
    }

    // ReSharper disable once ClassNeverInstantiated.Global
    public class VariableNode : INode
    {
        public readonly Token Token;

        public VariableNode(Token token)
        {
            Token = token;
        }

        public override string ToString() => $"VAR:{Token.Value}";
    }

    public class BinaryOpNode : INode
    {
        public readonly INode A;
        public readonly Token Op;
        public readonly INode B;

        public BinaryOpNode(INode a, Token op, INode b)
        {
            A = a;
            Op = op;
            B = b;
        }

        public override string ToString() =>
            $"{GetType().Name.Replace("Node", "")}({A}, {Op}, {B})";
    }

    public class LogicOpNode : INode
    {
        public readonly INode A;
        public readonly Token Op;
        public readonly INode B;

        public LogicOpNode(INode a, Token op, INode b)
        {
            A = a;
            Op = op;
            B = b;
        }

        public override string ToString() =>
            $"{GetType().Name.Replace("Node", "")}({A}, {Op}, {B})";
    }

    public class UnaryOpNode : INode
    {
        public readonly Token Op;
        public readonly INode Factor;

        public UnaryOpNode(Token op, INode factor)
        {
            Op = op;
            Factor = factor;
        }

        public override string ToString() =>
            $"{GetType().Name.Replace("Node", "")}({Op}, {Factor})";
    }

    public class VarCreationNode : INode
    {
        public readonly Token Type;
        public readonly Token Name;
        public readonly INode Value;

        public VarCreationNode(Token type, Token name, INode value)
        {
            Type = type;
            Name = name;
            Value = value;
        }

        public override string ToString() =>
            $"{GetType().Name.Replace("Node", "")}({Type.Value} {Name.Value} = {Value})";
    }

    public class VarAssignmentNode : INode
    {
        public readonly Token Name;
        public readonly INode Value;

        public VarAssignmentNode(Token name, INode value)
        {
            Name = name;
            Value = value;
        }

        public override string ToString() =>
            $"{GetType().Name.Replace("Node", "")}({Name.Value} = {Value})";
    }
}