﻿using System;
using System.Collections.Generic;
using System.Linq;
using lang.ala.Lexer;

namespace lang.ala.Parser
{
    public class Parser
    {
        private readonly string _code;
        private readonly Token[] _tokens;

        public Error Error;

        private int _pointer;

        private Token CurrentToken => _pointer < _tokens.Length
            ? _tokens[_pointer]
            : null;

        private void Advance()
        {
            _pointer++;
        }


        public Parser(string code, Token[] tokens)
        {
            _code = code;
            _tokens = tokens;
        }

        public INode Parse()
        {
            if (CurrentToken == null)
            {
                return null;
            }

            var result = Expr();
            if (CurrentToken == null) return result;
            Error = new Error(_code, "Invalid syntax", CurrentToken.Start, CurrentToken.End);
            _pointer = _tokens.Length;

            return result;
        }

        private INode Expr()
        {
            var variableTypes = new[] {"int", "real", "bool", "string"};
            if (CurrentToken != null && CurrentToken.Type == TokenType.KeyWord &&
                variableTypes.Contains(CurrentToken.Value))
            {
                var type = CurrentToken;
                Advance();
                if (CurrentToken == null || CurrentToken.Type != TokenType.Identifier)
                    return ParseError($"Invalid syntax: Unexpected token {CurrentToken}, Identifier is expected");
                var name = CurrentToken;
                Advance();
                if (CurrentToken == null || CurrentToken.Type != TokenType.Op || CurrentToken.Value != "ASSIGNMENT")
                    return ParseError($"Invalid syntax: Unexpected token {CurrentToken}, '=' is expected");
                Advance();
                return new VarCreationNode(type, name, Expr());
            }

            var pointer = _pointer;
            if (CurrentToken != null && CurrentToken.Type == TokenType.Identifier)
            {
                var name = CurrentToken;
                Advance();
                if (CurrentToken != null && CurrentToken.Type == TokenType.Op && CurrentToken.Value == "ASSIGNMENT")
                {
                    Advance();
                    return new VarAssignmentNode(name, Expr());
                }

                _pointer = pointer;
            }

            var result = ComparisonExpr();
            var operators = new[] {"AND", "LAZY_AND", "OR", "LAZY_OR"};
            while (CurrentToken != null && CurrentToken.Type == TokenType.Op &&
                   operators.Contains(CurrentToken.Value))
            {
                var token = CurrentToken;
                Advance();
                result = new LogicOpNode(result, token, ComparisonExpr());
            }

            return result;
        }

        private INode ComparisonExpr()
        {
            if (CurrentToken != null && CurrentToken.Type == TokenType.Op && CurrentToken.Value == "NOT")
            {
                var token = CurrentToken;
                Advance();
                return new UnaryOpNode(token, ComparisonExpr());
            }

            var result = ArithmeticExpr();
            var operators = new[] {"LESS", "LESS_OR_EQUALS", "GREATER", "GREATER_OR_EQUAL", "EQUALS"};
            while (CurrentToken != null && CurrentToken.Type == TokenType.Op &&
                   operators.Contains(CurrentToken.Value))
            {
                var token = CurrentToken;
                Advance();
                result = new BinaryOpNode(result, token, ArithmeticExpr());
            }

            return result;
        }

        private INode ArithmeticExpr()
        {
            var result = Term();
            var operators = new[] {"PLUS", "MINUS", "MULTIPLY", "DIVIDE"};
            while (CurrentToken != null && CurrentToken.Type == TokenType.Op &&
                   operators.Contains(CurrentToken.Value))
            {
                var token = CurrentToken;
                Advance();
                result = new BinaryOpNode(result, token, Term());
            }

            return result;
        }

        private INode Term()
        {
            var result = Factor();
            var operators = new[] {"MULTIPLY", "DIVIDE"};
            while (CurrentToken != null && CurrentToken.Type == TokenType.Op &&
                   operators.Contains(CurrentToken.Value))
            {
                var token = CurrentToken;
                Advance();
                result = new BinaryOpNode(result, token, Factor());
            }

            return result;
        }

        private INode Factor()
        {
            var operators = new[] {"PLUS", "MINUS"};
            if (CurrentToken != null && CurrentToken.Type == TokenType.Op &&
                operators.Contains(CurrentToken.Value))
            {
                var token = CurrentToken;
                Advance();
                var a = Factor();
                return new UnaryOpNode(token, a);
            }

            return Power();
        }

        private INode Power()
        {
            var result = Atom();
            while (CurrentToken != null && CurrentToken.Type == TokenType.Op && CurrentToken.Value == "POWER")
            {
                var token = CurrentToken;
                Advance();
                result = new BinaryOpNode(result, token, Atom());
            }

            return result;
        }

        private INode Atom()
        {
            if (CurrentToken == null) return null;
            var simpleNodes = new Dictionary<TokenType, string>
            {
                {TokenType.Integer, "Integer"},
                {TokenType.Real, "Real"},
                {TokenType.Bool, "Boolean"},
                {TokenType.String, "String"},
                {TokenType.Identifier, "Variable"}
            };

            if (simpleNodes.ContainsKey(CurrentToken.Type))
            {
                var token = CurrentToken;
                Advance();
                var type = Type.GetType("lang.ala.Parser." + simpleNodes.GetValueOrDefault(token.Type) + "Node");
                return type == null ? null : (INode) Activator.CreateInstance(type, token);
            }

            if (CurrentToken.Type == TokenType.ParenL)
            {
                Advance();
                var result = Expr();
                if (CurrentToken.Type != TokenType.ParenR)
                    return ParseError($"Invalid syntax: Unexpected token {CurrentToken}, ')' is expected");


                Advance();
                return result;
            }

            return ParseError($"Invalid syntax: Unexpected token {CurrentToken}");
        }

        private INode ParseError(string exception)
        {
            if (CurrentToken == null) return NeedTokenError();
            Error = new Error(
                _code,
                $"Parse Error: {exception}",
                CurrentToken.Start,
                CurrentToken.End);
            return null;
        }

        private INode NeedTokenError()
        {
            var lines = _code.Split('\r', '\n');
            var lastLine = lines.Last();
            Error = new Error(_code, "NeedTokenError",
                new Pointer {Pos = _code.Length, Line = lines.Length, Char = lastLine.Length + 1});
            _pointer = _tokens.Length;
            return null;
        }
    }
}