﻿using System;
using System.IO;
using System.Linq;
using CommandLine;
using lang.ala.Interpreter;

namespace lang.ala
{
    internal static class Program
    {
        private static Options _options;
        public static readonly VariablesTable GlobalVariablesTable = new VariablesTable();

        private static void Main(string[] args)
        {
            CommandLine.Parser.Default.ParseArguments<Options>(args).WithParsed(options =>
            {
                _options = options;
                if (options.SourceFile != null)
                {
                    if (File.Exists(options.SourceFile))
                    {
                        Run(File.ReadAllText(options.SourceFile).Trim());
                    }
                    else
                    {
                        Console.WriteLine("Source file doesn't exist");
                    }
                }
                else
                {
                    while (true)
                    {
                        Console.Write(">>>");
                        Run(Console.ReadLine()?.Trim());
                    }
                }
            });
        }

        private static void Run(string code)
        {
            var lexer = new Lexer.Lexer(code);
            var tokens = lexer.Tokens;
            if (lexer.Error != null || tokens == null) return;
            if (_options.ShowTokens) Console.WriteLine(string.Join('\n', tokens.ToList()));
            var parser = new Parser.Parser(code, tokens);
            var ast = parser.Parse();
            if (parser.Error != null || ast == null) return;
            if (_options.ShowAst) Console.WriteLine(ast);
            var interpreter = new Interpreter.Interpreter(code);
            var result = interpreter.Visit(ast, new Context("<program>") {VariablesTable = GlobalVariablesTable});
            if (!result.IsNull)
                Console.WriteLine($"result = {result}");
        }
    }
}