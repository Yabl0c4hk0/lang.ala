﻿namespace lang.ala.Lexer
{
    public class Pointer
    {
        public int Pos;
        public int Char;
        public int Line;

        public Pointer()
        {
            Pos = 0;
            Char = 1;
            Line = 1;
        }

        public Pointer(Pointer old)
        {
            Pos = old.Pos;
            Char = old.Char;
            Line = old.Line;
        }
    }
}