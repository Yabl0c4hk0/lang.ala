﻿using CommandLine;

namespace lang.ala
{
    // ReSharper disable once ClassNeverInstantiated.Global
    public class Options
    {
        [Option('s', "source", HelpText = "Your source file.")]
        // ReSharper disable once UnusedAutoPropertyAccessor.Global
        public string SourceFile { get; set; }

        [Option("show_tokens", HelpText = "Show tokens.")]
        // ReSharper disable once UnusedAutoPropertyAccessor.Global
        public bool ShowTokens { get; set; }
        [Option("show_ast", HelpText = "Show abstract syntax tree.")]
        // ReSharper disable once UnusedAutoPropertyAccessor.Global
        public bool ShowAst { get; set; }
    }
}